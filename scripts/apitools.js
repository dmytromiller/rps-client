'use strict';

var apiCallStarted; 

const baseurl = 'http://localhost:9000/rps/api/'; //http://dm.orienteering.dp.ua/rps/api/

async function sendAPIRequest(url, unique=false) {
	if(unique && (apiCallStarted >0)) {
		throw "API request is in progress";
	} else {
		apiCallStarted = Date.now();
		let response;
		let result, resultType;

		try {
			response = await fetch(url);
			
			let contType = response.headers.get('Content-Type');

			if(contType.includes('text/plain')) {
				resultType = 'text';
				result = await response.text();
			} else {
				resultType = 'json';
				result = await response.json();
			}

			} 
		catch (e) {
			resultType = 'text';
			result = 'API call failed completely: ' + e.message;
			response = {status:503, statusText:'Service Unavailable'};
		}
		finally {
			apiCallStarted = 0;
			return({status: response.status, statustext: response.statusText, resulttype: resultType, result: result});	
		}
	}
}

function commandUrl(command, parameters) {
	let url = baseurl + command + '?';
	let i = 0;
	
	for(let prop in parameters) {
		url += (i != 0) ? '&' : '';
		url += prop + '=' + parameters[prop];
		i++;
	}
	
	return url;	
}

function alertAPIErrorMsg(response) {
	let msg = 'API request returned an error, code: ' + response.status + ' (' + response.statustext + ')\n\n';
	msg += 'Details: ' + response.result;
	
	alert(msg);
}
